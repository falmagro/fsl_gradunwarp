# Copyright and Licenses

This is the [FSL gradunwarp package][fsl_gradunwarp]. 

It has been adapted from v 1.0.2 of [Human Connectome Project gradunwarp package][gradunwarp-hcp] which was forked from a ["no longer actively maintained" gradunwarp package][gradunwarp-ksubramz].

This versions contains changes made for and by the FSL team ([FSL][FSL]).

This fsl_gradunwarp package, including all examples, code snippets and attached documentation is covered by the MIT license from the original fork (as supplied below.)

# 3rd Party Code

## NiBabel

The nibabel package, including all examples, code snippets and attached
documentation is covered by the MIT license.

# The MIT License

  * Copyright (c) 2022-2032 [WU-Minn Human Connectome Project consortium][HCP]
  * Copyright (c) 2022-2032 Fidel Alfaro Almagro <falfaro.almagro@ndcn.ox.ac.uk>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

<!-- References -->

[fsl_gradunwarp]: https://git.fmrib.ox.ac.uk/falmagro/fsl_gradunwarp
[gradunwarp-hcp]: https://github.com/Washington-University/gradunwarp
[gradunwarp-ksubramz]: https://github.com/ksubramz/gradunwarp
[FSL]: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki
[HCP]: http://www.humanconnectome.org