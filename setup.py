from numpy.distutils.core import setup, Extension
from numpy.distutils.misc_util import get_numpy_include_dirs 
import os, sys

mods = ['fsl_gradunwarp.core.coeffs', 'fsl_gradunwarp.core.globals', 
        'fsl_gradunwarp.core.__init__', 'fsl_gradunwarp.__init__',
        'fsl_gradunwarp.core.utils',
        'fsl_gradunwarp.core.unwarp_resample',
        'fsl_gradunwarp.core.gradient_unwarp',
        'fsl_gradunwarp.core.tests.test_utils',
       ]

dats = [('fsl_gradunwarp/core/', ['fsl_gradunwarp/core/interp3_ext.c']),
        ('fsl_gradunwarp/core/', ['fsl_gradunwarp/core/legendre_ext.c']),
        ('fsl_gradunwarp/core/', ['fsl_gradunwarp/core/transform_coordinates_ext.c']),
       ]

# to build the C extension interp3_ext.c
ext1 = Extension('fsl_gradunwarp.core.interp3_ext',
                 include_dirs = get_numpy_include_dirs(),
                 sources = ['fsl_gradunwarp/core/interp3_ext.c'],
                 extra_compile_args=['-O3'])
# to build the C extension legendre_ext.c
ext2 = Extension('fsl_gradunwarp.core.legendre_ext',
                 include_dirs = get_numpy_include_dirs(),
                 sources = ['fsl_gradunwarp/core/legendre_ext.c'],
                 extra_compile_args=['-O3'])
# to build the C extension transform_coordinates_ext.c
ext3 = Extension('fsl_gradunwarp.core.transform_coordinates_ext',
                 include_dirs = get_numpy_include_dirs(),
                 sources = ['fsl_gradunwarp/core/transform_coordinates_ext.c'],
                 extra_compile_args=['-O3'])

scripts_cmd = ['fsl_gradunwarp/core/gradient_unwarp.py',]
        

def configuration(parent_package='', top_path=None):
    from numpy.distutils.misc_util import Configuration
    config = Configuration('',parent_package,top_path)
    config.add_data_files ( *dats )
    return config

setup(name='fsl_gradunwarp',
      version = '1.0.0',
      description = 'FSL version of Gradient Unwarping Package for Python/Numpy',
      author = 'Fidel Alfaro Almagro',
      py_modules  = mods,
      ext_modules = [ext1, ext2, ext3],
      scripts = scripts_cmd,
      configuration=configuration,
     )

