#!/usr/bin/env python
#
# gradient_unwarp_apply.py - Wrapper for gradient_unwarp that allows the use
#                            as a python library and also applies the unwarping
#
# Author: Fidel Alfaro Almagro
#

import os
import argparse as arg

from argparse import Namespace
from fsl.wrappers.misc  import fslroi
from fsl.wrappers.fnirt import convertwarp
from fsl.wrappers.fnirt import applywarp
from fsl_gradunwarp.core import (globals, coeffs, utils)
from fsl_gradunwarp.core.gradient_unwarp            import GradientUnwarpRunner as GUR
from fsl_gradunwarp.core.gradient_unwarp_half_voxel import GradientUnwarpRunner as GURHF

def gradient_unwarp_apply(WD, infile, outfile, owarp, vendor, gradcoeff,
                          fovmin=None, fovmax=None, numpoints=None, warp=False,
                          nojac=True, order=1, half=False, verbose=False):

    BaseName  = infile.split("/")[-1].split(".")[0]
    infile_3D = WD + "/" + BaseName + "_vol1.nii.gz"
    trilinear = WD + "/trilinear.nii.gz"
    fullWarp  = WD + "/fullWarp_abs.nii.gz"

    # do some validation
    if not os.path.exists(infile):
        raise IOError(infile + ' not found')
    if not os.path.exists(gradcoeff):
        raise IOError(gradcoeff + ' not found')

    # g and c are mutually exclusive arguments
    if gradcoeff.endswith('.coef'):
        args = Namespace(WD=WD, infile=infile_3D, outfile=trilinear, vendor=vendor,
                         coeffile=gradcoeff, warp=warp, nojac=nojac, order=order,
                         verbose=verbose)
    else: # ends with .grad or any other alternative
        args = Namespace(WD=WD, infile=infile_3D, outfile=trilinear, vendor=vendor,
                         gradfile=gradcoeff, warp=warp, nojac=nojac, order=order,
                         verbose=verbose)

    if fovmin != None:
        args['fovmin'] = fovmin
    if fovmax != None:
        args['fovmax'] = fovmax
    if numpoints != None:
        args['numpoints'] = numpoints

    if not os.path.exists(WD):
        os.makedirs(WD)

    fslroi(infile, infile_3D, 0, 1)

    # Allowing the use of grad_unwarp half voxel for legacy reasons
    if half:
        grad_unwarp = GURHF(args)
    else:
        grad_unwarp = GUR(args)

    grad_unwarp.run()
    grad_unwarp.write()

    convertwarp(abs=True, relout=True, ref=trilinear, warp1=fullWarp, out=owarp)
    applywarp(rel=True, interp="spline", paddingsize=1, src=infile,
              ref=infile_3D, w=owarp, out=outfile)
